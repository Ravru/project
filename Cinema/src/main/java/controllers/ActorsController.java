package controllers;

import models.Actor;
import services.ActorsService;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/actors")
public class ActorsController {
    @Inject
    private ActorsService actorsService;
    @Path("/all")
    @GET
    @Produces("application/json")
    public JsonArray getAll(){
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (Actor actor : actorsService.getAll()){
            builder.add(Json.createObjectBuilder().add("Id", actor.getIdActors()));
        }
        return builder.build();
    }
}
