package services;
import models.Actor;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class ActorsService {
    @PersistenceContext
    private EntityManager em;
    public List<Actor> getAll(){
        TypedQuery<Actor> query = em.createQuery("select u from Actor u", Actor.class);
        return  query.getResultList();
    }

}
