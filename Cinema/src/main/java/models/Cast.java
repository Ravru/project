package models;

import javax.persistence.*;

@Entity
@Table(name = "cast", schema = "cinema")
public class Cast {
    private int idCast;
    private int actorsId;
    private int filmsId;

    @Id
    @Column(name = "id_Cast")
    public int getIdCast() {
        return idCast;
    }

    public void setIdCast(int idCast) {
        this.idCast = idCast;
    }

    @Basic
    @Column(name = "Actors_id")
    public int getActorsId() {
        return actorsId;
    }

    public void setActorsId(int actorsId) {
        this.actorsId = actorsId;
    }

    @Basic
    @Column(name = "Films_id")
    public int getFilmsId() {
        return filmsId;
    }

    public void setFilmsId(int filmsId) {
        this.filmsId = filmsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cast that = (Cast) o;

        if (idCast != that.idCast) return false;
        if (actorsId != that.actorsId) return false;
        if (filmsId != that.filmsId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idCast;
        result = 31 * result + actorsId;
        result = 31 * result + filmsId;
        return result;
    }
}
