package models;

import javax.persistence.*;

@Entity
@Table(name = "genre", schema = "cinema")
public class Genre {
    private int idGenre;
    private String name;

    @Id
    @Column(name = "id_Genre")
    public int getIdGenre() {
        return idGenre;
    }

    public void setIdGenre(int idGenre) {
        this.idGenre = idGenre;
    }

    @Basic
    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Genre that = (Genre) o;

        if (idGenre != that.idGenre) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idGenre;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
