package models;

import javax.persistence.*;

@Entity
@Table(name = "actors", schema = "cinema")
public class Actor {
    private int idActors;
    private String name;
    private Integer age;

    @Id
    @Column(name = "id_Actors")
    public int getIdActors() {
        return idActors;
    }

    public void setIdActors(int idActors) {
        this.idActors = idActors;
    }

    @Basic
    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Age")
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Actor that = (Actor) o;

        if (idActors != that.idActors) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (age != null ? !age.equals(that.age) : that.age != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idActors;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (age != null ? age.hashCode() : 0);
        return result;
    }
}
